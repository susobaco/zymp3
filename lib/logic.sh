#!/bin/bash

source /opt/zymp3/config/zymp3.conf

#Codec format of audio file
#libmp3lame (mp3), libvorbis (ogg), flac (flac)
CODEC="" #this gets set in he 'changeCodec' function

#CHECK FOR YAD OR ZENITY AND DEFAULT TO ONE
if [ ! -f /usr/bin/yad ];then
  SET_GUI_BIN="zenity"
elif [ ! -f /usr/bin/zenity ];then
  SET_GUI_BIN="yad"
else
  SET_GUI_BIN="${SET_GUI_BIN}"
fi

if [ ! -f /usr/bin/ffmpeg ];then
  SET_CONV_TOOL="avconv"
else
  SET_CONV_TOOL="ffmpeg"
fi

changeCodec(){
  if [ "${EXTENSION}" = "ogg" ];then
    CODEC="libvorbis"
  elif [ "${EXTENSION}" = "mp3" ];then
    CODEC="libmp3lame"
  elif [ "${EXTENSION}" = "flac" ];then
    CODEC="flac"
  fi
}

#PROGRESS BAR
backend()
{

  youtube-dl \
  --output=${VIDEOFILE} --format=18 "$1" | ${SET_GUI_BIN}\
  --progress \
  --pulsate --title="Descargando..." \
  --text="Descargando el vídeo, por favor, espere..." --auto-close

  if [[ $? == 1 ]];then
    exit 0;
  fi

  if [ "${USE_FILE_BROWSER}" = "no" ];then
    if [ ! -f $VIDEOFILE ];then
      ${SET_GUI_BIN} \
      --error \
      --text "No se puede convertir el vídeo porque no existe, 
      es probable que no se haya descargado"."
      exit 0;

    elif [ -f $VIDEOFILE ];then
      changeCodec
      ${SET_CONV_TOOL} -i $VIDEOFILE -acodec ${CODEC} -ac 2 -ab ${BITRATE}k \
       -vn -y "${CONVERTED}/$2" | ${SET_GUI_BIN} \
      --progress\
      --pulsate\
      --title="Convirtiendo..." \
      --text="Convirtiendo el vídeo.."\
      --auto-close
      if [[ $? != 0 ]];then
        exit 0;
      fi

      rm ${VIDEOFILE}

    else
      echo -e "\e[1;31mERROR: Parece que el archivo de vídeo se ha descargado correctamente, 
                              pero no se ha convertido. \e[0m"

      ${SET_GUI_BIN}\
       --error \
       --text "Parece que el archivo de vídeo se ha descargado correctamente, 
               pero no se ha convertido."
    fi

  else
    if [ ! -f $VIDEOFILE ];then
      ${SET_GUI_BIN}\
       --error \
       --text "No se puede convertir el vídeo porque no existe, 
               probablemente no se pudo descargar."
      exit 0;

    elif [ -f $VIDEOFILE ];then
      ${SET_CONV_TOOL}\
      -i $VIDEOFILE\
      -acodec ${CODEC}\
      -ac 2 -ab ${BITRATE}k\
      -vn -y "$2" | ${SET_GUI_BIN} \
      --progress\
      --pulsate\
      --title="Convirtiendo..." \
      --text="Convirtiendo el vídeo..."\
      --auto-close

      if [[ $? != 0 ]];then
        exit 0;
      fi
      rm ${VIDEOFILE}

    else
      echo -e\
       "\e[1;31mERROR: Parece que el archivo de vídeo se ha descargado correctamente, 
       pero no se ha convertido. \e[0m"

      ${SET_GUI_BIN} --error \
      --text "Parece que el archivo de vídeo se ha descargado correctamente, 
      pero no se ha convertido."
    fi
  fi

}


#PASTE YOUTUBE LINK - WINDOW
gui()
{
  VIDURL=$(${SET_GUI_BIN} --title="Zyogg 0.1.7" \
  --height=${URL_BOX_HEIGHT} \
  --width=${URL_BOX_WIDTH}\
  --entry  \
  --text "Pega el link de youtube aquí: ")


  if  [[ $? == 0 ]];then
    if [[ ${VIDURL} == *"https://www.youtube.com/watch?v="* ]];then
      gui2

    elif [[ ${VIDURL} == *"https://youtu.be/"* ]];then
      gui2
    
    else
      ${SET_GUI_BIN} --error --text "URL inválida"
    fi

  else
     exit 0;

  fi




}



#ENTER MP3 FILE NAME OR OPEN FILEBROWSER
#CAN BE SWITCHED IN CONFIG

gui2()
{

  if [ "${USE_FILE_BROWSER}" = "no" ];then
    AUDIOFILENAME=$(${SET_GUI_BIN} --title="Nombre del archivo" \
    --height=${FILENAME_BOX_HEIGHT} \
    --width=${FILENAME_BOX_WIDTH} \
    --entry --text "Nombre del archivo: ")

    if [[ $? == 0 ]];then
      dconvert
    else
      exit 0;
    fi

  else
    FILEBROWSER=$(zenity --file-selection --directory \
    --title= "¿Dónde se guarda el archivo ogg?? " \
    --filename=/home/$USER/Music/ \
    --file-filter='
     MP3 files (mp3) | *.mp3',
    'OGG files (ogg) | *.ogg',
    'FLAC (flac) | *.flac' \
    --save --confirm-overwrite)

    if [[ $? == 0 ]];then
      dconvert
    else
      exit 0;
    fi
  fi

}




#CHECK IF FILEBROWSER VAR IS SET
#IF NOT SET, DEFAULT TO MANUAL TO {MUSICDIR}
open()
{

  if [ "${USE_FILE_BROWSER}" = "no" ];then
    if [[ $? == 0 ]];then
    xdg-open "${MUSICDIR}${AUDIOFILENAME}.${EXTENSION}"
    else
      exit 0;
    fi

  else
    if [[ $? == 0 ]];then
      xdg-open "${FILEBROWSER}"
    else
      exit 0;
    fi

  fi


}

#MOVE MP3 FILE TO MUSICDIR
#IF MUSICDIR DOES NOT EXIST, CREATE IT
move()
{
  if [ "${USE_FILE_BROWSER}" = "no" ];then
    if [ -d "${MUSICDIR}" ];then
      mv -v "${CONVERTED}/${AUDIOFILENAME}.${EXTENSION}" "${MUSICDIR}"
    elif [ ! -d "${MUSICDIR}" ];then
      mkdir "${MUSICDIR}"
      mv -v "${CONVERTED}/${AUDIOFILENAME}.${EXTENSION}" "${MUSICDIR}"

    fi


  fi
}


#CHECK IF MP3 FILE EXISTS
checkFile()
{

  if [ "${USE_FILE_BROWSER}" = "no" ];then
    if [ -f "${MUSICDIR}${AUDIOFILENAME}.${EXTENSION}" ];then
      notify-send "${AUDIOFILENAME}.${EXTENSION} se ha guardado en ${MUSICDIR}"
      ${SET_GUI_BIN} --question \
      --title="Hey!" \
      --text="Se ha movido $AUDIOFILENAME.${EXTENSION} a $MUSICDIR, ¿quiere reproducirlo ahora?"
      if [[ $? != 0 ]];then
        exit 0;
      fi

    elif [ ! -f "${MUSICDIR}${AUDIOFILENAME}.${EXTENSION}" ];then
      ${SET_GUI_BIN} --error \
      --text\
       "El archivo mp3 no existe. La descarga ha fallado 
       o el vídeo no se ha convertido a mp3 correctamente."
    fi

  else
    if [ -f "${FILEBROWSER}.${EXTENSION}" ];then
      notify-send "${FILEBROWSER}.${EXTENSION} ha sido guardado"
      ${SET_GUI_BIN} --question \
      --title="Hey!" \
      --text="${FILEBROWSER}.${EXTENSION} ha sido guardado, ¿quire reproducirlo ahora?"
      if [[ $? == 0 ]];then
        open
      else
        exit 0;
      fi

    elif [ ! -f "${FILEBROWSER}.${EXTENSION}" ];then
      ${SET_GUI_BIN} --error \
      --text\
       "El archivo convertido no existe. La descarga ha fallado 
       o el vídeo no se ha convertido correctamente."
    fi
  fi


}


dconvert()
{

  #PASS VIDEO URL AND FILENAME.MP3 TO BACKEND FUNCTION
  if [ "${USE_FILE_BROWSER}" = "no" ];then
    backend "${VIDURL}" "${AUDIOFILENAME}.${EXTENSION}"
    move
    checkFile
    open


  else
    backend "${VIDURL}" "${FILEBROWSER}.${EXTENSION}"
    checkFile
  fi




}
