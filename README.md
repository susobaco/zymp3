## Zyogg
### Una interfaz para youtube-dl y ffmpeg/avconv. Fork de zymp3 ( https://github.com/silvernode/zymp3 )



### Dependencias  


* youtube-dl (requido)
* zenity (requirdo)
* yad (opcional)
* xdg-utils (requirido)
* libnotify (opcional)
* ffmpeg o avconvert (requirido)



### Install from Master  

Instale git desde su repositorio de distribución 
para ubuntu, ejecutar:  
```
sudo apt-get install git 
```

A continuación, use git para hacer un clon de este repositorio
Esto descargará una copia del repositorio en un directorio llamado "zyogg":  

```
git clone https://gitlab.com/susobaco/zymp3.git
```
Cambie al nuevo directorio
```
cd zyogg
```

Y ahora ejecute el script de instalación como root
  
```
./install.sh
```

### Actualizar  

Para actualizar a una nueva versión, primero cambie al directorio de repositorios: :  
```
cd zymp3
```
Luego ejecute el comando pull:
```
git pull
```
Esto fusionará cualquier cambio que se haya hecho a los archivos y lo pondrá al día.
Alternativamente puede ejecutar el script de actualización (que sólo ejecuta git pull)
Añadí esto en caso de que tengas mala memoria. 
```
./update.sh
```

### Uninstall  

Para desinstalar, ejecute el script de desinstalación que se encuentra en el repositorio o en el directorio de instalación: 

```
./uninstall.sh
```

### Config  

Se puede encontrar un archivo de configuración en: 

#### /opt/zymp3/config  

------------------------

```  
#!/bin/bash
#config.sh

# Zymp3 Configuration File

# FILE PATHS

# choose your own install path
INSTALLPATH="/opt/zymp3"

# You can change this but icon may not appear
IMGDIR="/usr/share/pixmaps"

# Desktop icon directory ( default recommended)
DESKTOPFILEDIR="/usr/share/applications"

# Default gui - switch between zenity or yad
SET_GUI_BIN="yad"

# ogg, mp3, flac
EXTENSION="ogg"


# 96 - 320 (kilobytes)
BITRATE="192"


# Output directory for audio files
# must have a trailing slash /
MUSICDIR="/home/$USER/Music/"

# VIDEO TMP DIRECTORY

# You shouldn't need to change this but you can
VIDEOFILE=/tmp/youtube-dl-$RANDOM-$RANDOM.flv
CONVERTED=/tmp

# USE FILEBROWSER TO NAME AND SAVE MP3

USE_FILE_BROWSER="no" #options- yes, no

# GUI SETTINGS

# Change dimensions of the URL and filename dialogs
URL_BOX_HEIGHT="64"

URL_BOX_WIDTH="512"

FILENAME_BOX_HEIGHT="64"

FILENAME_BOX_WIDTH="326"
```  
-----------------
Por favor, envíe cualquier problema que encuentre, gracias